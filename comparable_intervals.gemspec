# frozen_string_literal: true

lib = File.expand_path('lib', __dir__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'comparable_intervals/version'

Gem::Specification.new do |spec|
  spec.name          = 'comparable_intervals'
  spec.version       = ComparableIntervals::VERSION
  spec.authors       = ['Mihai Muntenas']
  spec.email         = ['mihai.muntenas@gmail.com']

  spec.summary       = 'A simple library that provides specific operations on a set of intervals.'
  spec.description   = 'Provides sorting, filtering, merging and overlap detection of a set of intervals.'
  spec.homepage      = 'https://bitbucket.org/mihaimuntenas/intervals'
  spec.license       = 'MIT'

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files         = Dir.chdir(File.expand_path(__dir__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  end
  spec.bindir        = 'exe'
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']

  spec.add_development_dependency 'bundler', '~> 1.17'
  spec.add_development_dependency 'rake', '~> 10.0'
  spec.add_development_dependency 'rspec', '~> 3.0'
  spec.add_development_dependency 'rubocop', '~> 0.75'
  spec.add_development_dependency 'rubocop-performance', '~> 1.5'
  spec.add_development_dependency 'rubocop-rspec', '~> 1.36'
end
