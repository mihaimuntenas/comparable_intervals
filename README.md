# ComparableIntervals

A simple library that provides functionality to perform specific operations on a set of intervals.
An interval is defined as being a tuple of sorted `Comparable` values.

```ruby
[[8, 10], [11, 12]]
[[12:00:00, 14:00:00],[15:00:00, 17:00:00]]
```

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'intervals'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install intervals

## Usage

A set of intervals is defined and validated to be a `Enumberable` of tuples (implicit `Array`) of `Comparable` values, of the same type.
This means that the type of the values should implement the `<=>` operator and include Ruby's `Comparable` module.

The library expose the functionality through the class `ComparableIntervals::Tuples`, both at class and instance levels.

>The class level methods **assume** the set of tuples is already sorted; sorting is provided at class level through the `sort` method.

>Though through the examples `Integer` values are used (for brevity), do note that `Time` is also a `Comparable`.

### Sorting

Class method `sort` takes a set of tuples as parameter and sorts them ascending by the beginning of the interval (first value in the array).

```ruby
ComparableIntervals::Tuples.sort([[15, 17], [8, 10], [14, 16], [10, 12]]) 
=> [[8, 10], [10, 12], [14, 16], [15, 17]]
```

>The instances of class `ComparableIntervals::Tuples` will keep the inner `tuples` sorted at all times.

### Overlap detection

Class method `ovelaps?` takes a set of **sorted** tuples as parameter and determines if any of the contained intervals overlap; comparison is done excluding edges.

```ruby
ComparableIntervals::Tuples.overlaps?([[8, 10], [10, 12], [14, 16], [15, 17]]) 
=> true # [14, 16] and [15, 17] overlap
```

### Filtering

Class method `filter` takes a set of **sorted** tuples as parameter and filters them through the set of intervals provided through the parameter `sieve`.

```ruby
ComparableIntervals::Tuples.filter([[8, 10], [10, 12], [14, 16], [15, 17]], [[8, 11], [11, 14]])
=> [[8, 10], [10, 12]]
```

>NOTE: The `sieve` set will be internally `merge`'d before the filtering.

### Merging

Class method `merge` takes a set of **sorted** tuples as parameter and reduces them, by "merging" adjacent ones.

```ruby
ComparableIntervals::Tuples.merge([[8, 10], [10, 12], [14, 16], [15, 17]]) 
=> [[8, 12], [14, 17]]
```

### Generating interval sets

Class method `tuples_for` is a helper method that assits with generation of same size interval sets, within a specific given range; it provides an additional option to "fill" the set with the "reminder" interval (enabled by default), if the range length does not exactly divide to the size of the desired interval.

```ruby
# ComparableIntervals::Tuples.tuples_for(from, to, step, fill: true)
ComparableIntervals::Tuples.tuples_for(1, 20, 3) # fill is true by default
=> [[1, 4], [4, 7], [7, 10], [10, 13], [13, 16], [16, 19], [19, 20]]

ComparableIntervals::Tuples.tuples_for(1, 20, 3, fill: false)
=> [[1, 4], [4, 7], [7, 10], [10, 13], [13, 16], [16, 19]]
```

or using `Time` values
```ruby
ComparableIntervals::Tuples.tuples_for(Time.parse('01:00'), Time.parse('20:00'), 3*3600, fill: false)
=> [[01:00:00, 04:00:00], [04:00:00, 07:00:00], [07:00:00, 10:00:00], [10:00:00, 13:00:00], [13:00:00, 16:00:00], [16:00:00, 19:00:00]]
```

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

The test suite of choice is `rspec` and code style is ensured by `rubocop`.

## Contributing

Bug reports and pull requests are welcome on BitBucket at https://bitbucket.org/mihaimuntenas/comparable_intervals. This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [Contributor Covenant](http://contributor-covenant.org) code of conduct.

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).

## Code of Conduct

Everyone interacting in the Intervals project’s codebases, issue trackers, chat rooms and mailing lists is expected to follow the [code of conduct](https://bitbucket.org/mihaimuntenas/comparable_intervals/src/master/CODE_OF_CONDUCT.md).
