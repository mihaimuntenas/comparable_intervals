# frozen_string_literal: true

RSpec.describe ComparableIntervals::Tuples do
  describe '.filter' do
    let(:input) do
      (1..20).collect { |i| [i, i + 1] }
    end
    let(:sieve) do
      [[1, 5], [10, 15]]
    end
    let(:expected) do
      ((1..4).to_a + (10..14).to_a).collect { |i| [i, i + 1] }
    end

    it 'returns the intervals covered by the filter' do
      expect(described_class.filter(input, sieve)).to eq(expected)
    end
  end

  describe '.merge' do
    let(:input) do
      (1..rand(1..20)).collect { |i| [i, i + rand(1..10)] }
    end
    let(:expected) do
      [[
        input.map(&:first).sort.min,
        input.map(&:last).sort.max
      ]]
    end

    it 'returns the "merged" set of intervals' do
      expect(described_class.merge(input)).to eq(expected)
    end
  end

  describe '.overlaps?' do
    let(:input_with) do
      (1..rand(1..20)).collect { |i| [i, i + 2 * (i % 2)] }
    end
    let(:input_without) do
      (1..rand(1..20)).collect { |i| [i, i + 1] }
    end

    it 'is truthy if any of the tuples overlap' do
      expect(described_class).to be_overlaps(input_with)
    end

    it 'is falsey if none of the tuples overlap' do
      expect(described_class).not_to be_overlaps(input_without)
    end
  end

  describe '.sort' do
    let(:input) do
      (1..rand(1..20)).collect { |i| [i - rand(1..10), i + rand(1..10)] }
    end
    let(:expected) do
      input.map(&:first).sort
    end

    it 'orders the tuples ascending' do
      expect(described_class.sort(input).map(&:first)).to eq(expected)
    end
  end

  describe '.tuples_for' do
    let(:from) { rand(1..10) }
    let(:to) { from + 10 }
    let(:step) { rand(1..3) }
    let(:expected) do
      result = []
      t_next = from
      while t_next + step <= to
        result << [t_next, t_next + step]
        t_next += step
      end
      result
    end
    let(:expected_filled) do
      expected.last.last != to ? expected + [[expected.last.last, to]] : expected
    end

    it 'without filling, generates tuples for the given params' do
      expect(described_class.tuples_for(from, to, step, fill: false)).to eq(expected)
    end

    it 'with filling, generates tuples for the given params' do
      expect(described_class.tuples_for(from, to, step, fill: true)).to eq(expected_filled)
    end
  end

  describe '.validate_schema_of' do
    let(:a_non_comparable) { Class.new }
    let(:a_comparable) { Class.new { include Comparable } }
    let(:bad_input_len) do
      (1..rand(1..20)).collect { |i| [i, i + rand(10), i + rand(5)] }
    end
    let(:bad_input_not_same_type) do
      (1..rand(1..20)).collect { |i| [i, (i + rand(10)).to_s] }
    end
    let(:bad_input_not_comparable) do
      (1..rand(1..20)).collect { |_i| [a_non_comparable.new, a_non_comparable.new] }
    end
    let(:input) do
      (1..rand(1..20)).collect { |_i| [a_comparable.new, a_comparable.new] }
    end

    it 'fails if param is not an Enumerable' do
      expect do
        described_class.validate_schema_of(2)
      end.to raise_error(ArgumentError, 'Invalid argument value; expected enumeration.')
    end

    it 'fails if enumeration does not contain tuples' do
      expect do
        described_class.validate_schema_of(bad_input_len)
      end.to raise_error(ArgumentError, 'Invalid argument value; expected enumeration of pairs.')
    end

    it 'fails if enumeration does not contain tuples of same type' do
      expect do
        described_class.validate_schema_of(bad_input_not_same_type)
      end.to raise_error(ArgumentError, 'Invalid argument value; expected comparable values.')
    end

    it 'fails if enumeration does not contain comparable values' do
      expect do
        described_class.validate_schema_of(bad_input_not_comparable)
      end.to raise_error(ArgumentError, 'Invalid argument value; expected comparable values.')
    end

    it 'passes if enumeration contains tupes of comparable values' do
      expect do
        described_class.validate_schema_of(input)
      end.not_to raise_error
    end
  end

  describe '#tuples=' do
    let(:inst) { described_class.new }
    let(:input) do
      (1..rand(1..20)).collect { |i| [i - rand(1..10), i + rand(1..10)] }
    end
    let(:expected) do
      input.map(&:first).sort
    end

    it 'keeps the tuples sorted' do
      inst.tuples = input
      expect(inst.tuples.map(&:first)).to eq(expected)
    end
  end
end
