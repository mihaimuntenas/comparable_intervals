# frozen_string_literal: true

require_relative 'comparable_intervals/version'
require_relative 'comparable_intervals/tuples'

# Namespace module
module ComparableIntervals
end
