# frozen_string_literal: true

# Namespace module
module ComparableIntervals
  # Utility class that is able to "reduce" or "merge" a set of comparable value tuples (or intervals)
  #   to an unique encompasing set, wich "covers" the same intervals.
  class Tuples
    # @!attribute [rw] tuples
    #   @return [Enumerable] The set of tuples/intervals
    attr_reader :tuples

    # @param [Enumerable] tups Set of tuples/intervals
    def initialize(tups = [])
      send(:tuples=, tups)
    end

    # @return [Enumerable] Set of tuples/intervals
    # @param [Enumerable] tups Set of tuples/intervals
    def tuples=(value)
      self.class.validate_schema_of(value)
      @tuples = self.class.sort(value.clone)
    end

    # @return [Enumerable] Merged set of tuples/intervals
    def merge
      self.class.merge(tuples)
    end

    # @return [Tuples] Merges the tuples/intervals
    def merge!
      @tuples = merge
      self
    end

    # @return [Boolean] Tests if the tuples have overlaps
    def overlaps?
      self.class.overlaps?(tuples)
    end

    class << self
      # @return [Enumerable] Fitlered set of tuples/intervals covered by the sieve
      # @param [Enumerable] tuples Set of sorted tuples/intervals to filter
      # @param [Enumerable] sieve Set of sorted tuples/intervals to use as filter
      def filter(tuples, sieve)
        merged = merge(sieve)
        tuples.each_with_object([]) do |i, accu|
          accu << i if merged.any? { |f| i[0] >= f[0] && f[1] >= i[1] }
        end
      end

      # @return [Enumerable] Set of sorted tuples/intervals
      # @param [Enumerable] tuples Set of sorted tuples/intervals
      def merge(tuples)
        tuples.each_with_object([]) do |i, accu|
          if accu.empty?
            accu << i.clone
          else
            accu.last[1] >= i[0] ? accu.last[1] = [accu.last[1], i[1]].max : accu << i.clone
          end
        end
      end

      # @return [Boolean] Tests if the tuples have overlaps
      # @param [Enumerable] tuples Set of sorted tuples/intervals
      def overlaps?(tuples)
        result = false
        tuples.each_with_index do |i, idx|
          next if idx.zero?

          result ||= tuples[idx - 1][1] > i[0]
        end
        result
      end

      # @return [Enumerable] Sorted tuples
      # @param [Enumerable] tuples Set of tuples/intervals
      def sort(tuples)
        tuples.sort { |a, b| (r = a[0] <=> b[0]).zero? ? a[1] <=> b[1] : r }
      end

      # @return [Nothing]
      # @param [Enumerable] tuples Set of tuples/intervals
      # @raise [ArgumentError] If the tuples do not match the "schema"
      def validate_schema_of(tuples)
        raise ArgumentError, 'Invalid argument value; expected enumeration.' unless tuples.class.include?(Enumerable)

        validate_form_of(tuples) unless tuples.empty?
        validate_type_of(tuples) unless tuples.empty?
      end

      # @return [Enumerable] A set of comparable value tuples for the provided params
      # @param [Comparable] from Start of the interval span
      # @param [Comparable] to End of the interval span
      # @param [Comparable] step Duration/Interval step
      # @param [Boolean] fill: Option to fill "gap" at the end
      def tuples_for(from, to, step, fill: true)
        return [[from, to]] if (to - from) <= step

        result = generate(((to - from) / step).to_i, from, step)
        result << [result.last.last, to] if fill && result.last.last != to

        result
      end

      private

      # @return [Enumerable] A set of comparable value tuples for the provided params
      # @param [Integer] count Number of tuples to generate
      # @param [Comparable] from Start of the interval span
      # @param [Comparable] step Duration/Interval step
      def generate(count, from, step)
        (1..count).collect do |i|
          [from + (i - 1) * step, from + i * step]
        end
      end

      # @return [Nothing]
      # @param [Enumerable] tuples Set of tuples/intervals
      def validate_form_of(tuples)
        test = tuples.all? { |t| t.count == 2 }
        raise ArgumentError, 'Invalid argument value; expected enumeration of pairs.' unless test
      end

      # @return [Nothing]
      # @param [Enumerable] tuples Set of tuples/intervals
      def validate_type_of(tuples)
        klass = tuples.first.first.class
        same_klass = tuples.all? { |t| t.first.is_a?(klass) && t.last.is_a?(klass) }
        test = same_klass && klass.include?(Comparable)
        raise ArgumentError, 'Invalid argument value; expected comparable values.' unless test
      end
    end
  end
end
