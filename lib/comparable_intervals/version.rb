# frozen_string_literal: true

# Namespace module
module ComparableIntervals
  VERSION = '0.1.0'
end
